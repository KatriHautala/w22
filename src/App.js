import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
      <h1>W22 Hello country example</h1><br/>
      <ComponentName country = "Finland"/>
      <ComponentName country = "Sweden"/>
    </div>
  );
}

export default App;
